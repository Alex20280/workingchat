package Server;

public interface RoomInterface {

    void connectToRoom();

    void disconnectFromRoom ();

    void broadcast (String message, UserThread excludeUser);

}
