package Server;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;


//This thread handles connection for each connected client, so the server
public class UserThread extends Thread {
    private Socket socket;

    private ServerInterface serverInterface;
    private RoomInterface roomInterface;
    private PrintWriter writer;

    public UserThread(Socket socket, ServerInterface serverInterface) {
        this.socket = socket;
        this.serverInterface = serverInterface;
    }
    HashMap<String, UserThread> user = new HashMap<String, UserThread>();

    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);

            String userName = reader.readLine();
            user.put(userName,null);

            serverInterface.connectToChat(userName, this);
            String serverMessage = "New user connected: " + userName;
            roomInterface.broadcast(serverMessage, this);

            String clientMessage;

            do {
                clientMessage = reader.readLine();
                serverMessage = "[" + userName + "]: " + clientMessage;
                roomInterface.broadcast(serverMessage, this);

            } while (!clientMessage.equals("bye"));

            serverInterface.disconnectFromChat(userName, this);
            socket.close();

            serverMessage = userName + " has quitted.";

        } catch (IOException ex) {
            System.out.println("Error in UserThread: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            writer.close();
        }
    }

    void sendMessage(String message) {
        writer.println(message);
    }

}








//package Server;
//
//        import java.io.*;
//        import java.net.Socket;
//
//
////This thread handles connection for each connected client, so the server
//public class UserThread extends Thread {
//    private Socket socket;
//    private ChatServer server; //
//    private PrintWriter writer;
//
//    public UserThread(Socket socket, ChatServer server) {
//        this.socket = socket;
//        this.server = server;
//    }
//
//    public void run() {
//        try {
//            InputStream input = socket.getInputStream();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
//
//            OutputStream output = socket.getOutputStream();
//            writer = new PrintWriter(output, true);
//
//            String userName = reader.readLine();
//            server.connect(userName);
//
//            String serverMessage = "New user connected: " + userName;
//            server.broadcast(serverMessage, this);
//
//            String clientMessage;
//
//            do {
//                clientMessage = reader.readLine();
//                serverMessage = "[" + userName + "]: " + clientMessage;
//                server.broadcast(serverMessage, this);
//
//            } while (!clientMessage.equals("bye"));
//
//            server.disconnect(userName, this);
//            socket.close();
//
//            serverMessage = userName + " has quitted.";
//            server.broadcast(serverMessage, this);
//
//        } catch (IOException ex) {
//            System.out.println("Error in UserThread: " + ex.getMessage());
//            ex.printStackTrace();
//        }
//    }
//
//    //Sends a message to the client.
//    void sendMessage(String message) {
//        writer.println(message);
//    }
//
//}
