package Server;

public interface ServerInterface {

    void connectToChat(String name, UserThread userThread);

    void disconnectFromChat(String userName, UserThread aUser);

    void createRoom ();

}
