package Server;

import Client.ChatClient;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.stream.Stream;

public class ChatServer implements ServerInterface, RoomInterface {
    private int port;
    ChatClient chatClient;

    private HashMap<String, UserThread> userMap = new HashMap<>(); // username and thread
    private HashMap<String, String> chatRooms = new HashMap<>(); //  username and room name

    public ChatServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {

        int port = 1024;

        ChatServer server = new ChatServer(port);
        server.execute();
    }

    public void execute() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Chat Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New user connected");

                UserThread newUser = new UserThread(socket, this);

                System.out.println("The number of online users: " + userMap.size());

                System.out.println("Active Chat rooms: " + chatRooms.size());

                newUser.start();
            }

        } catch (IOException ex) {
            System.out.println("Error in the server: " + ex.getMessage());
            ex.printStackTrace();
        }
    }


    @Override
    public void connectToChat(String name, UserThread userThread) {
        userMap.put(name, userThread);
    }

    @Override
    public void disconnectFromChat(String userName, UserThread aUser) {
        boolean removed = userMap.keySet().remove(userName);
        if (removed) {
            userMap.remove(aUser);
            System.out.println("The user " + userName + " quitted");
            System.out.println("The number of online users: " + userMap.size());
        }
    }

    @Override
    public void createRoom() {

    }

    @Override
    public void connectToRoom() {

    }

    @Override
    public void disconnectFromRoom() {

    }

    @Override
    public void broadcast(String message, UserThread excludeUser) {
        for (UserThread aUser : userMap.values()){
            if (aUser != excludeUser) {
                aUser.sendMessage(message);
            }
        }

    }

}