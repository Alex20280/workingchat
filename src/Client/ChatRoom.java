package Client;

import Server.RoomInterface;
import Server.UserThread;

public class ChatRoom implements RoomInterface {

    @Override
    public void connectToRoom() {

    }

    @Override
    public void disconnectFromRoom() {

    }

    @Override
    public void broadcast(String message, UserThread excludeUser) {

    }
}
