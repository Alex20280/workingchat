package Client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class WriteThread extends Thread {
    private PrintWriter writer;
    private Socket socket;
    private ChatClient client;

    public String chatRoomName;
    String userName;

    public WriteThread(Socket socket, ChatClient client) {
        this.socket = socket;
        this.client = client;

        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException ex) {
            System.out.println("Error getting output stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void run() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name:" );
        userName = scanner.next();
        System.out.println("Enter the chat room:" );
        chatRoomName = scanner.next();
        System.out.println("You are in a chat room" + chatRoomName);

        client.setUserName(userName);

        writer.println(userName);
        writer.print(chatRoomName);

        String text;

        do {
            text = scanner.nextLine();
            writer.println(text);

        } while (!text.equals("bye"));

        try {
            System.out.println("Good bye " + userName);
            socket.close();
        } catch (IOException ex) {

            System.out.println("Error writing to server: " + ex.getMessage());
        }finally {
            writer.close();
        }
    }

}